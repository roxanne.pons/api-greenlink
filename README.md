# api-greenlink

## INSTALL:
- Git clone
- Composer install
- Create .env file and copy conf-greenlink.txt (in documentation folder) + change credentials 
- php bin/console doctrine:database:create
- php bin/console doctrine:schema:update --force 
- Create folder jwt in config
- openssl genrsa -out config/jwt/private.pem -aes256 4096 (command line)
- Pswd (Ligne 42 dans .env) copy and past in command line
- openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem (command line)
- Pswd (Ligne 42 dans .env) copy and past in command line - again
- Generate RSA file

## http://localhost:8888/api-greenlink/public/


## Test in insomnia app
- Import file greenlink.json (in documentation folder)
- (Make sure url is the right one)
- change dans POST /users les credentials
- Go on http://localhost:8888/phpmyadmin/
- Eable user (edit: enable 0 to 1)
- in POST /login_check put your credentials and send 
- Copy token you are given
- In GET /users paste token (in Bearer Token authentification mode)

## Auto generate documentation (by api-platform):
- http://localhost:8888/api-greenlink/public/api
