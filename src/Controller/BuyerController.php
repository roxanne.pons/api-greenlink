<?php

namespace App\Controller;

use App\Entity\Buyer;
use App\Entity\MemberShipType;
use App\Repository\BuyerRepository;
use App\Service\Sender;
use App\Service\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class BuyerController extends AbstractController
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    // get json request params and convert them to array
    private function getJsonParamsRequest()
    {
        $request = Request::createFromGlobals();

        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        return $data;
    }

    /**
     * @Route("/api/buyer/signup", name="buyer_signup", methods={"POST"})
     */
    public function signin(BuyerRepository $buyerRepository, UserPasswordEncoderInterface $encoder): Response
    {

        if (!$data = $this->getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }



        $lastName = $data['lastName'];
        $firstName = $data['firstName'];
        $email = $data['email'];
        $phone = str_replace(' ', '+', $data['phone']);
        $password = $data['password'];
        $passwordConfirmation = $data['passwordConfirmation'];

        // check password and passwordConfirm equality
        if ($password !==  $passwordConfirmation)
            return new JsonResponse(['status' => 500, 'message' => 'password Confirmation not match'], 500);

        if (
            $buyerRepository->findOneBy(['email' => $email]) ||
            $buyerRepository->findOneBy(['phone' => $phone])
        ) {
            return new JsonResponse(['status' => 500, 'message' => 'email | phone are duplicated'], 500);
        }

        $buyer = new Buyer();
        $buyer->setFirstName($firstName);
        $buyer->setLastName($lastName);
        $buyer->setEmail($email);
        $buyer->setPhone($phone);

        // password crypt
        $passwordEncoded = $encoder->encodePassword($buyer, $password);
        $buyer->setPassword($passwordEncoded);


        $this->manager->persist($buyer);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
                'result' => [
                    'id' => $buyer->getId(),
                    'firstName' => $buyer->getFirstName(),
                    'lastName' => $buyer->getLastName(),
                    'email' => $buyer->getEmail()
                ]

            ],
            200
        );
    }

    /**
     * @Route("/api/buyer/login", name="buyer_login", methods={"POST"})
     */
    public function login(BuyerRepository $buyerRepository, UserPasswordEncoderInterface $encoder): Response
    {

        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if ((!isset($data['email']) || !$email = $data['email']) ||  (!isset($data['email']) || !$password = $data['password']))
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        $buyer = $buyerRepository->findOneBy(['email' => $email]);

        if (!$buyer)
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);

        // password check
        if ($encoder->isPasswordValid($buyer, $password))
            return new JsonResponse(
                [
                    'status' => 200,
                    'result' => [
                        'id' => $buyer->getId(),
                        'firstName' => $buyer->getFirstName(),
                        'email' => $buyer->getEmail(),
                        'phone' => $buyer->getPhone()
                    ]
                ],
                200
            );
        else
            return new JsonResponse(['status' => 404], 404);
    }



    /**
     * @Route("/api/buyer/email/confirm/request", name="buyer_email_confirm_request", methods={"POST"})
     */
    public function emailConfirmRequest(BuyerRepository $buyerRepository, UserPasswordEncoderInterface $encoder, Sender $sender): Response
    {
        if (!$data = $this->getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }

        $buyerId = $data['buyerId'];

        $buyer = $buyerRepository->find($buyerId);

        if (!$buyer)
            return new JsonResponse(['status' => 500, 'message' => 'not found'], 500);

        if ($buyer->getIsValidEmail()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        $buyer->setEmailCodeCheck(rand(100000, 999999));

        $this->manager->persist($buyer);
        $this->manager->flush();

        $sender->email(
            "Greenlink : Email validation",
            $buyer->getEmail(),
            'emails/email_confirm.html.twig',
            ['code' => $buyer->getEmailCodeCheck()]
        );


        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }


    /**
     * @Route("/api/buyer/email/validation", name="buyer_email_validation", methods={"POST"})
     */
    public function emailValidation(BuyerRepository $buyerRepository, UserPasswordEncoderInterface $encoder, Sender $sender): Response
    {
        if (!$data = $this->getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }

        $buyerId = $data['buyerId'];
        $code = $data['code'];

        $buyer = $buyerRepository->find($buyerId);

        if (!$buyer)
            return new JsonResponse(['status' => 500, 'message' => 'not found'], 500);

        if ($buyer->getIsValidEmail()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        if ($buyer->getEmailCodeCheck() != $code) {
            return new JsonResponse(['status' => 404, 'message' => 'invalid code'], 404);
        }


        $buyer->setIsValidEmail(true);

        $this->manager->persist($buyer);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }


    /**
     * @Route("/api/buyer/phone/confirm/request", name="buyer_phone_confirm_request", methods={"POST"})
     */
    public function phoneConfirmRequest(BuyerRepository $buyerRepository, Sender $sender): Response
    {
        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if (!isset($data['buyerId']) || !$buyerId = $data['buyerId'])
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$buyer = $buyerRepository->find($buyerId))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);


        if ($buyer->getIsValidPhone()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        $buyer->setPhoneCodeCheck(rand(100000, 999999));

        $this->manager->persist($buyer);
        $this->manager->flush();

        $sender->sms(
            $buyer->getPhone(),
            $buyer->getPhoneCodeCheck()
        );


        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }

    /**
     * @Route("/api/buyer/phone/validation", name="buyer_phone_validation", methods={"POST"})
     */
    public function phoneValidation(BuyerRepository $buyerRepository,  Sender $sender): Response
    {
        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if ((!isset($data['buyerId']) || !$buyerId = $data['buyerId']) || (!isset($data['code']) || !$code = $data['code']))
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$buyer = $buyerRepository->find($buyerId))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);


        if ($buyer->getIsValidPhone()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        if ($buyer->getIsValidPhone()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        if ($buyer->getPhoneCodeCheck() != $code) {
            return new JsonResponse(['status' => 404, 'message' => 'invalid code'], 404);
        }


        $buyer->setIsValidPhone(true);

        $this->manager->persist($buyer);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }
}
