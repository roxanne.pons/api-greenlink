<?php

namespace App\Controller;

use App\Entity\Buyer;
use App\Entity\Farm;
use App\Entity\MemberShipType;
use App\Repository\BuyerRepository;
use App\Repository\FarmRepository;
use App\Repository\TypeOperationRepository;
use App\Service\Sender;
use App\Service\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class FarmController extends AbstractController
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    // get json request params and convert them to array
    private function getJsonParamsRequest()
    {
        $request = Request::createFromGlobals();

        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        return $data;
    }
    /**
     * @Route("/api/farm/signup/second", name="farm_signup_second", methods={"POST"})
     */
    public function signinSecond(FarmRepository $farmRepository, TypeOperationRepository $typeOperationRepository, UserPasswordEncoderInterface $encoder): Response
    {

        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }



        $idFarm = $data['idFarm'];
        $name = $data['name'];
        //$address = $data['address'];
        //$city = $data['city'];
        //$postalCode = $data['postalCode'];
        //$country = $data['country'];
        $typeOperationId = $data['typeOperationId'];

        if (
            !$farm = $farmRepository->find($idFarm)
        ) {
            return new JsonResponse(['status' => 404, 'message' => 'Farm not found'], 404);
        }

        if (
            !$typeOperation = $typeOperationRepository->find($typeOperationId)
        ) {
            return new JsonResponse(['status' => 404, 'message' => "Type operation doesn't exist"], 404);
        }

        /** @var $farm Farm */
        $farm->setName($name);
        //$farm->setAddress($address);
        //$farm->setCity($city);
        //$farm->setPostalCode($postalCode);
        //$farm->setCountry($country);
        $farm->setTypeOperation($typeOperation);


        $this->manager->persist($farm);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
                'result' => [
                    'id' => $farm->getId(),
                    'name' => $farm->getName(),
                    'email' => $farm->getEmail()
                ]

            ],
            200
        );
    }


    /**
     * @Route("/api/farm/signup/first", name="farm_signup_first", methods={"POST"})
     */
    public function signinFirst(FarmRepository $farmRepository, TypeOperationRepository $typeOperationRepository, UserPasswordEncoderInterface $encoder): Response
    {

        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }



        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $email = $data['email'];
        $email = $data['email'];
        //quick fix pour numero internationaux
        $phone = str_replace(' ', '+', $data['phone']);
        $password = $data['password'];
        $passwordConfirmation = $data['passwordConfirmation'];

        // check password and passwordConfirm equality
        if ($password !==  $passwordConfirmation)
            return new JsonResponse(['status' => 500, 'message' => 'password Confirmation not match'], 500);

        if (
            $farmRepository->findOneBy(['email' => $email]) ||
            $farmRepository->findOneBy(['phone' => $phone])
        ) {
            return new JsonResponse(['status' => 500, 'message' => 'email | phone are duplicated'], 500);
        }


        $farm = new Farm();
        $farm->setEmail($email);
        $farm->setPhone($phone);
        $farm->setFirstName($firstName);
        $farm->setLastName($lastName);

        // password crypt
        $passwordEncoded = $encoder->encodePassword($farm, $password);
        $farm->setPassword($passwordEncoded);


        $this->manager->persist($farm);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
                'result' => [
                    'id' => $farm->getId(),
                    'firstName' => $farm->getFirstName(),
                    'lastName' => $farm->getLastName(),
                    'email' => $farm->getEmail()
                ]

            ],
            200
        );
    }

    /**
     * @Route("/api/farm/login", name="farm_login", methods={"POST"})
     */
    public function login(FarmRepository $farmRepository, UserPasswordEncoderInterface $encoder): Response
    {

        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if ((!isset($data['email']) || !$email = $data['email']) ||  (!isset($data['email']) || !$password = $data['password']))
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$farm = $farmRepository->findOneBy(['email' => $email]))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);

        // password check
        if ($encoder->isPasswordValid($farm, $password))
            return new JsonResponse(
                [
                    'status' => 200,
                    'result' => ['id' => $farm->getId(), 'firstName' => $farm->getName()]
                ],
                200
            );
        else
            return new JsonResponse(['status' => 404], 404);
    }



    /**
     * @Route("/api/farm/email/confirm/request", name="farm_email_confirm_request", methods={"POST"})
     */
    public function emailConfirmRequest(FarmRepository $farmRepository, Sender $sender): Response
    {
        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if (!isset($data['farmId']) || !$farmId = $data['farmId'])
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$farm = $farmRepository->find($farmId))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);


        if ($farm->getIsValidEmail()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        $farm->setEmailCodeCheck(rand(100000, 999999));

        $this->manager->persist($farm);
        $this->manager->flush();

        $sender->email(
            "Greenlink : Email validation",
            $farm->getEmail(),
            'emails/email_confirm.html.twig',
            ['code' => $farm->getEmailCodeCheck()]
        );


        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }


    /**
     * @Route("/api/farm/email/validation", name="farm_email_validation", methods={"POST"})
     */
    public function emailValidation(FarmRepository $farmRepository, Sender $sender): Response
    {
        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if ((!isset($data['farmId']) || !$farmId = $data['farmId']) || (!isset($data['code']) || !$code = $data['code']))
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$farm = $farmRepository->find($farmId))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);


        if ($farm->getIsValidEmail()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        if ($farm->getIsValidEmail()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        if ($farm->getEmailCodeCheck() != $code) {
            return new JsonResponse(['status' => 404, 'message' => 'invalid code'], 404);
        }


        $farm->setIsValidEmail(true);

        $this->manager->persist($farm);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }

    /**
     * @Route("/api/farm/phone/confirm/request", name="farm_phone_confirm_request", methods={"POST"})
     */
    public function phoneConfirmRequest(FarmRepository $farmRepository, Sender $sender): Response
    {
        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if (!isset($data['farmId']) || !$farmId = $data['farmId'])
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$farm = $farmRepository->find($farmId))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);


        if ($farm->getIsValidPhone()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        $farm->setPhoneCodeCheck(rand(100000, 999999));

        $this->manager->persist($farm);
        $this->manager->flush();

        $sender->sms(
            $farm->getPhone(),
            $farm->getPhoneCodeCheck()
        );


        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }

    /**
     * @Route("/api/farm/phone/validation", name="farm_phone_validation", methods={"POST"})
     */
    public function phoneValidation(FarmRepository $farmRepository,  Sender $sender): Response
    {
        if (!$data = Tools::getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);
        }


        if ((!isset($data['farmId']) || !$farmId = $data['farmId']) || (!isset($data['code']) || !$code = $data['code']))
            return new JsonResponse(['status' => 500, 'message' => "missing parameters"], 500);


        if (!$farm = $farmRepository->find($farmId))
            return new JsonResponse(['status' => 404, 'message' => "not found"], 404);


        if ($farm->getIsValidPhone()) {
            return new JsonResponse(['status' => 403, 'message' => 'Already valid'], 403);
        }

        if ($farm->getPhoneCodeCheck() != $code) {
            return new JsonResponse(['status' => 404, 'message' => 'invalid code'], 404);
        }


        $farm->setIsValidPhone(true);

        $this->manager->persist($farm);
        $this->manager->flush();

        return new JsonResponse(
            [
                'status' => 200,
            ],
            200
        );
    }
}
