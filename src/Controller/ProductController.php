<?php

namespace App\Controller;

use App\Repository\CategoryProductRepository;
use App\Service\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends AbstractController
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    // get json request params and convert them to array
    private function getJsonParamsRequest()
    {
        $request = Request::createFromGlobals();

        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        return $data;
    }

    /**
     * @Route("/api/categories/get", name="categories_get_by_id", methods={"POST"})
     */
    public function categoriesGetById(CategoryProductRepository $categoryProductRepository): Response
    {
        if (!$data = $this->getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }

        $categoryId = $data['categoryId'];
        //
        if (isset($data['categoryParents']) && $data['categoryParents'] == true) {
            //'category' => null = pas de parent -> 1st level
            $categories = $categoryProductRepository->findBy(['category' => null]);
        } else {
            //recup les parents des autres categories
            $category = $categoryProductRepository->find($categoryId);

            if (!$category)
                return new JsonResponse(['status' => 404, 'message' => 'not found'], 404);
            $categories = $categoryProductRepository->findBy(['category' => $category]);
        }


        $categoriesId = [];
        foreach ($categories as $category) {
            $categoriesId[] = ['id' => $category->getId(), 'name' => $category->getName()];
        }

        return new JsonResponse(
            [
                'status' => 200,
                'result' => $categoriesId
            ],
            200
        );
    }
}
