<?php

namespace App\Controller;

use App\Entity\Buyer;
use App\Entity\Card;
use App\Entity\Farm;
use App\Entity\Order;
use App\Entity\Product;
use App\Repository\CategoryProductRepository;
use App\Service\Tools;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class OrderController extends AbstractController
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    // get json request params and convert them to array
    private function getJsonParamsRequest()
    {
        $request = Request::createFromGlobals();

        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        return $data;
    }

    /**
     * @Route("/api/orders/add", name="order_add", methods={"POST"})
     */
    public function orderAdd(): Response
    {
        if (!$data = $this->getJsonParamsRequest()) {
            return new JsonResponse(['status' => 500], 500);
        }

        $card = json_decode($data['card'], 1);

        $card = $card['card'];

        $buyer = end($card);

        array_pop($card);

        $cards = [];
        $total = 0;
        foreach ($card as $item) {

            foreach ($item as $c) {
                $cards[] = [
                    'product' => $c['id'],
                    'farm' => $c['farm']['id'],
                    'quantity' => $c['quantity'],
                    'price' => $c['price'],
                ];

                $total += $c['price'] * $c['quantity'];
            }
        }

        $order = new Order();
        $order->setBuyer($this->manager->getRepository(Buyer::class)->find($buyer)); // @todo dynamisation
        $order->setTotal($total);

        $this->manager->persist($order);
        $this->manager->flush();

        foreach ($cards as $c2) {
            $cardObj = new Card();
            $cardObj->setFarm($this->manager->getRepository(Farm::class)->find($c2['farm']));
            $cardObj->setProduct($this->manager->getRepository(Product::class)->find($c2['product']));
            $cardObj->setQuantity($c2['quantity']);
            $cardObj->setPu($c2['price']);
            $cardObj->setOrdre($order);

            $this->manager->persist($cardObj);
            $this->manager->flush();
        }

        return new JsonResponse(
            [
                'status' => 200,
                'result' => [
                    'order' => [
                        'id' => $order->getId(),
                        'total' => $order->getTotal()
                    ]
                ]
            ],
            200
        );
    }
}
