<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BuyerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Api\FilterInterface;

/**
 * @ApiResource(
 *     collectionOperations = {
 *      "get"= {
 *          "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *            "normalization_context"= {"groups" = {"read"}}
 *     },
 *      "post" = {
 *            "denormalization_context"= {"groups" = {"write"}},
 *          "normalization_context"= {"groups" = {"read"}},
 *     },
 *
 *     "buyer-signup"={
 *              "security"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *              "method"="POST",
 *              "path"="/api/buyer/signup",
 *              "controller"=App\Controller\BuyerController::class,
 *              "denormalization_context"={"groups"={"write"}},
 *              "validation_groups"={"write"}
 *          }
 *     },
 *     itemOperations = {
 *      "get"={
 *              "normalization_context"= {"groups" = {"read"}},
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *          },
 *     "put" = {
 *                "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *              "denormalization_context"= {"groups" = {"update"}}
 *           }
 *     }
 * )
 * @ApiFilter(PropertyFilter::class)
 * @ORM\Entity(repositoryClass=BuyerRepository::class)
 * @method string getUserIdentifier()
 */
class Buyer implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"read"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"read"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=40, unique=true)
     * @Assert\Email(groups={"write"})
     * @Groups({"read", "update"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $emailCodeCheck;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=15, unique=true)
     * @Groups({"read", "update"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     * @Groups({"read"})
     */
    private $phoneCodeCheck;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $deliveryAddress;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups({"read"})
     */
    private $deliveryCity;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"read"})
     */
    private $deliveryPostalCode;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"read"})
     */
    private $country;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isValidEmail = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isValidPhone = false;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="buyer")
     */
    private $orders;


    public function __construct()
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(new \DateTime('now'));
        }
        $this->orders = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailCodeCheck(): ?string
    {
        return $this->emailCodeCheck;
    }

    public function setEmailCodeCheck(?string $emailCodeCheck): self
    {
        $this->emailCodeCheck = $emailCodeCheck;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneCodeCheck(): ?string
    {
        return $this->phoneCodeCheck;
    }

    public function setPhoneCodeCheck(?string $phoneCodeCheck): self
    {
        $this->phoneCodeCheck = $phoneCodeCheck;

        return $this;
    }

    public function getDeliveryAddress(): ?string
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?string $deliveryAddress): self
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?string $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    public function getDeliveryPostalCode(): ?string
    {
        return $this->deliveryPostalCode;
    }

    public function setDeliveryPostalCode(?string $deliveryPostalCode): self
    {
        $this->deliveryPostalCode = $deliveryPostalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getRoles()
    {
        // TODO: Implement getRoles() method.
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }

    public function getIsValidEmail(): ?bool
    {
        return $this->isValidEmail;
    }

    public function setIsValidEmail(bool $isValidEmail): self
    {
        $this->isValidEmail = $isValidEmail;

        return $this;
    }

    public function getIsValidPhone(): ?bool
    {
        return $this->isValidPhone;
    }

    public function setIsValidPhone(?bool $isValidPhone): self
    {
        $this->isValidPhone = $isValidPhone;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setBuyer($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getBuyer() === $this) {
                $order->setBuyer(null);
            }
        }

        return $this;
    }
}
