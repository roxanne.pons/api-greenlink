<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Api\FilterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     collectionOperations = {
 *      "get"= {
 *          "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *            "normalization_context"= {"groups" = {"read"}}
 *     },
 *      "post" = {
 *            "denormalization_context"= {"groups" = {"write"}},
 *          "normalization_context"= {"groups" = {"read"}},
 *          },
 *
 *     },
 *
 *     itemOperations = {
 *      "get"={
 *              "normalization_context"= {"groups" = {"read"}},
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *          },
 *     "put" = {
 *                "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *              "denormalization_context"= {"groups" = {"update"}}
 *           }
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "farm": "exact"
 * })
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups({"read", "update", "write"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"read", "update", "write"})
     */
    private $unity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     * @Groups({"read", "update", "write"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "update", "write"})
     */
    private $stock;

    /**
     * @ORM\Column(type="text")
     * @Groups({"read", "update", "write"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $reduction;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read", "update", "write"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read", "update", "write"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=CategoryProduct::class, inversedBy="products")
     * @Groups({"read", "update", "write"})
     */
    private $categoryProduct;

    /**
     * @ORM\ManyToOne(targetEntity=Farm::class, inversedBy="products")
     * @Groups({"read", "update", "write"})
     */
    private $farm;

    /**
     * @ORM\OneToMany(targetEntity=Card::class, mappedBy="product")
     */
    private $cards;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $photos;

    public function __construct()
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(new \DateTime('now'));
        }
        $this->cards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnity(): ?string
    {
        return $this->unity;
    }

    public function setUnity(string $unity): self
    {
        $this->unity = $unity;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?string
    {
        return $this->stock;
    }

    public function setStock(string $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getReduction(): ?int
    {
        return $this->reduction;
    }

    public function setReduction(?int $reduction): self
    {
        $this->reduction = $reduction;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCategoryProduct(): ?CategoryProduct
    {
        return $this->categoryProduct;
    }

    public function setCategoryProduct(?CategoryProduct $categoryProduct): self
    {
        $this->categoryProduct = $categoryProduct;

        return $this;
    }

    public function getFarm(): ?Farm
    {
        return $this->farm;
    }

    public function setFarm(?Farm $farm): self
    {
        $this->farm = $farm;

        return $this;
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setProduct($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->removeElement($card)) {
            // set the owning side to null (unless already changed)
            if ($card->getProduct() === $this) {
                $card->setProduct(null);
            }
        }

        return $this;
    }

    public function getPhotos(): ?string
    {
        return $this->photos;
    }

    public function setPhotos(?string $photos): self
    {
        $this->photos = $photos;

        return $this;
    }
}
