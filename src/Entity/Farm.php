<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FarmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Api\FilterInterface;

/**
 * @ApiResource(
 *     collectionOperations = {
 *      "get"= {
 *          "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *            "normalization_context"= {"groups" = {"read"}}
 *     },
 *      "post" = {
 *            "denormalization_context"= {"groups" = {"write"}},
 *          "normalization_context"= {"groups" = {"read"}},
 *     },
 *
 *     "buyer-signup"={
 *              "security"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *              "method"="POST",
 *              "path"="/api/farm/signup",
 *              "controller"=App\Controller\FarmController::class,
 *              "denormalization_context"={"groups"={"write"}},
 *              "validation_groups"={"write"}
 *          }
 *     },
 *     itemOperations = {
 *      "get"={
 *              "normalization_context"= {"groups" = {"read"}},
 *              "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *          },
 *     "put" = {
 *                "access_control"="is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_WRITER')",
 *              "denormalization_context"= {"groups" = {"update"}}
 *           }
 *     }
 * )
 * @ApiFilter(PropertyFilter::class)
 * @ORM\Entity(repositoryClass=FarmRepository::class)
 * @method string getUserIdentifier()
 */
class Farm implements \Symfony\Component\Security\Core\User\UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Email(groups={"write"})
     * @Groups({"read", "update"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $emailCodeCheck;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=15)
     * @Groups({"read", "update", "write"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $phoneCodeCheck;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $country;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read", "update", "write"})
     */
    private $homeDelivery;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read", "update", "write"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read", "update", "write"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=TypeOperation::class, inversedBy="farms")
     * @Groups({"read", "update", "write"})
     */
    private $typeOperation;

    /**
     * @ORM\ManyToOne(targetEntity=MemberShipType::class, inversedBy="farms")
     * @Groups({"read", "update", "write"})
     */
    private $memberShipType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isValidEmail = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isValidPhone = false;

    /**
     * @ORM\Column(type="string", length=30)
     * @Groups({"read", "update", "write"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=30)
     * @Groups({"read", "update", "write"})
     */
    private $lastName;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="farm")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity=Card::class, mappedBy="farm")
     */
    private $cards;

    public function __construct()
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(new \DateTime('now'));
        }
        $this->products = new ArrayCollection();
        $this->cards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailCodeCheck(): ?string
    {
        return $this->emailCodeCheck;
    }

    public function setEmailCodeCheck(?string $emailCodeCheck): self
    {
        $this->emailCodeCheck = $emailCodeCheck;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneCodeCheck(): ?string
    {
        return $this->phoneCodeCheck;
    }

    public function setPhoneCodeCheck(?string $phoneCodeCheck): self
    {
        $this->phoneCodeCheck = $phoneCodeCheck;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }


    public function getHomeDelivery(): ?bool
    {
        return $this->homeDelivery;
    }

    public function setHomeDelivery(bool $homeDelivery): self
    {
        $this->homeDelivery = $homeDelivery;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTypeOperation(): ?TypeOperation
    {
        return $this->typeOperation;
    }

    public function setTypeOperation(?TypeOperation $typeOperation): self
    {
        $this->typeOperation = $typeOperation;

        return $this;
    }

    public function getMemberShipType(): ?MemberShipType
    {
        return $this->memberShipType;
    }

    public function setMemberShipType(?MemberShipType $memberShipType): self
    {
        $this->memberShipType = $memberShipType;

        return $this;
    }

    public function getIsValidEmail(): ?bool
    {
        return $this->isValidEmail;
    }

    public function setIsValidEmail(bool $isValidEmail): self
    {
        $this->isValidEmail = $isValidEmail;

        return $this;
    }

    public function getIsValidPhone(): ?bool
    {
        return $this->isValidPhone;
    }

    public function setIsValidPhone(?bool $isValidPhone): self
    {
        $this->isValidPhone = $isValidPhone;

        return $this;
    }

    public function getRoles()
    {
        // TODO: Implement getRoles() method.
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setFarm($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getFarm() === $this) {
                $product->setFarm(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setFarm($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->removeElement($card)) {
            // set the owning side to null (unless already changed)
            if ($card->getFarm() === $this) {
                $card->setFarm(null);
            }
        }

        return $this;
    }
}
