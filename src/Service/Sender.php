<?php


namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;


class Sender extends AbstractController
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function email($subject, $mail, $template, $params = array(), $attach = null, $from = false, $replyTo = false)
    {

        $content =  $this->renderView(
            $template,
            $params
        );
        return $this->emailSansTemplate($subject, $mail, $content, $attach, $from, $replyTo);
    }

    public function emailSansTemplate($subject, $mail, $content, $attach = null, $from = array(), $replyTo = false)
    {

        if (!$from) {
            $from = new Address($_ENV['MAIL_SENDER_ADDR'], $_ENV['MAIL_SENDER_NAME']);
        } elseif (is_array($from)) {
            foreach ($from as $key => $val) {
                $from = new Address($key, $val);
                break;
            }
        } else {
            $from = new Address($from);
        }
        $email = (new Email())
            ->from($from)
            ->to($mail)
            ->subject($subject)
            ->html($content);
        if ($replyTo) {
            $email->replyTo($replyTo);
        }

        if ($attach) {
            if (is_array($attach)) {
                foreach ($attach as $a) {
                    if (is_array($a)) {
                        $email->attach($a["content"], $a["fileName"]);
                    } else {
                        if (is_file($this->getParameter('project_dir') . $a)) {
                            $email->attachFromPath($this->getParameter('project_dir') . $a);
                        }
                    }
                }
            } else {
                if (is_file($this->getParameter('project_dir') . $attach)) {
                    $email->attachFromPath($this->getParameter('project_dir') . $attach);
                }
            }
        }
        try {
            $this->mailer->send($email);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function sms($mobile, $text, $from = null)
    {


        if (substr($mobile, 0, 2) == "00") {
            $mobile = "+" . substr($mobile, 2);
        } else if (substr($mobile, 0, 1) == "0") {
            $mobile = "+41" . substr($mobile, 1);
        }

        if (!$from) $from = $_ENV['SMS_FROM'];

        $token = $_ENV['SMS_TOKEN_API'];

        $headers = array();
        $headers[] = "Authorization: Bearer " . $token;
        $headers[] = 'Content-Type: application/json';
        $params = array(
            CURLOPT_URL => $_ENV['SMS_URL_API'],
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => json_encode(array(

                'From' => $from,
                'To' => $mobile,
                'Text' => $text
            )),
            CURLOPT_SSL_VERIFYPEER => false
        );
        $ch = curl_init();
        curl_setopt_array($ch, $params);
        $result = curl_exec($ch);
        if (curl_error($ch)) {
            //echo 'Error:' . curl_error($ch);
            return false;
        }
        curl_close($ch);
        return true;
    }
}
