<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\Request;

class Tools
{
    public static function getJsonParamsRequest()
    {
        $request = Request::createFromGlobals();

        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        return $data;
    }

}