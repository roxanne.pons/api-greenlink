<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\UserApi;
use App\Security\TokenGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegisterSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    private $tokenGenerator;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGenerator $tokenGenerator)
    {

        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    public static function getSubscribedEvents()
    {
        return [
            ViewEvent::class => ['userRegister', EventPriorities::PRE_WRITE]
        ];
    }

    public function userRegister(ViewEvent $event)
    {;
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();


        if (!$user instanceof UserApi || !in_array($method, [Request::METHOD_POST])) {
            return;
        }

        //It is an User , we need to hash password here
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $user->getPassword())
        );

        $user->setConfirmationToken($this->tokenGenerator->getRandomSecureToken());

        $user->setRoles(['ROLE_WRITER']);

        // Send email here.
        //$this->mailer->sendConfirmationEmail($user);


    }
}
