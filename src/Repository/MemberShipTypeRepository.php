<?php

namespace App\Repository;

use App\Entity\MemberShipType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MemberShipType|null find($id, $lockMode = null, $lockVersion = null)
 * @method MemberShipType|null findOneBy(array $criteria, array $orderBy = null)
 * @method MemberShipType[]    findAll()
 * @method MemberShipType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberShipTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MemberShipType::class);
    }

    // /**
    //  * @return MemberShipType[] Returns an array of MemberShipType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MemberShipType
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
